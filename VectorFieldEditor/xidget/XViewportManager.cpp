#include "SViewportManager.h"

vfe::SViewportManager::SViewportManager() : vfe::Slate()
{
}

vfe::SViewportManager::SViewportManager(sf::RenderWindow* window) : vfe::Slate(window)
{
}

vfe::SViewportManager::~SViewportManager()
{
}

void vfe::SViewportManager::create(int index, sf::FloatRect& viewport)
{
    if (viewports[index])
    {
        sf::View* del = viewports[index];
        viewports[index] = new sf::View(viewport);
        delete del;
    }
    else
        viewports[index] = new sf::View(viewport);
}

void vfe::SViewportManager::setAsCurrentView(int index)
{
    pWindow->setView((*viewports[index]));
}

void vfe::SViewportManager::newViewport(sf::FloatRect& viewport)
{
    viewports.push_back(&sf::View(viewport));
}

void vfe::SViewportManager::newViewport(sf::View* view)
{
    viewports.push_back(view);
}

void vfe::SViewportManager::removeLastViewport()
{
    viewports.pop_back();
}

void vfe::SViewportManager::removeViewportById(int index)
{
    viewports.erase(viewports.begin() + index);
}
