#pragma once
#include "Slate.h"

namespace vfe
{
    class SText : public vfe::Slate
    {
    public:
        SText();
        SText(sf::RenderWindow* window);
        virtual ~SText();

// Draw function
        virtual void draw();

        // Pure OnClicked event function, custom body
        virtual void OnClicked(sf::Vector2i mPos);

        // Pure OnHover event function, custom body
        virtual void OnHover(sf::Vector2i mPos);

        // Get button text
        std::string getText();
        // Set button text
        void setText(std::string txt);

        // Get button color
        sf::Color getColor();
        // Set button color
        void setColor(sf::Color color);

        // Get slate type
        virtual SlateType getType();

        virtual void operator=(Slate& slate);
        
    protected:
        //
    private:
        std::string text;
        sf::Color color;
    };
}
