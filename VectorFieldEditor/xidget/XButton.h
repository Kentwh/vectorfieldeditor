#pragma once
#include "XBox.h"

namespace vfe
{
    class XButton : public vfe::XBox
    {
    public:
        XButton();
        XButton(Xidget xidget);
        virtual ~XButton();

        // Draw function
        virtual void draw();

        // Callback OnClicked event function
        virtual void OnClicked(ClickCallback clickCallback);
    
        //Callback OnHover event function
        virtual void OnHover(HoverCallback hoverCallback);

        // Set button content
        void setContent(Xidget xidget);

        // Get button content
        Xidget getContent();

        // Get xidget type
        virtual XidgetType getType();

        virtual void operator=(Xidget& xidget);
    protected:
        //
    private:
        Xidget xidget;
    };
}
