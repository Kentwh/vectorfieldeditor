#pragma once
#include "Slate.h"

namespace vfe
{
    class SViewportManager : public Slate
    {
    public:
        SViewportManager();
        SViewportManager(sf::RenderWindow* window);
        virtual ~SViewportManager();

        // For recreation or post-initialization of viewport
        void create(int index, sf::FloatRect& viewport);

        // Set indexed viewport as current view (if pWindow is avaliable. If not, an error will occur)
        void setAsCurrentView(int index);

        // Add a new viewport by sf::View object
        void newViewport(sf::FloatRect& viewport);

        // Add a new viewport by sf::View object
        void newViewport(sf::View* view);

        // Remove back last viewport
        void removeLastViewport();

        // Remove viewport by id (index)
        void removeViewportById(int index);

    protected:
        //
    private:
        std::vector<sf::View*> viewports;
    };
}

