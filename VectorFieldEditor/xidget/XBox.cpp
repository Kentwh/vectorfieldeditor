#include "XBox.h"

XBox::XBox()
{
    //
}

XBox::XBox(bool border)
{
    setBorderState(false);
    setFillColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    setBorderColor(glm::vec4(1.0f, 1.0f, 1.0f, 0.0f));
}

XBox::XBox(bool border, glm::vec4 fillColor)
{
    setBorderState(border);
    setFillColor(fillColor);
}

XBox::XBox(bool border, glm::vec4 fillColor, glm::vec4 borderColor)
{
    setBorderState(border);
    setFillColor(fillColor);
    setBorderColor(borderColor);
}

XBox::XBox(glm::vec2 pos) : Xidget(pos)
{
}

XBox::XBox(glm::vec2 pos, glm::vec2 size) : Xidget(pos, size)
{
}

XBox::XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh) : Xidget(pos, size, mesh)
{
}

XBox::XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState) : Xidget(pos, size, mesh, fadeState)
{
}

XBox::XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState) : Xidget(pos, size, mesh, fadeState, disabledState)
{
}

XBox::XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState, bool border) : Xidget(pos, size, mesh, fadeState, disabledState)
{
    XBox(border);
}

XBox::XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState, bool border, glm::vec4 fillColor) : Xidget(pos, size, mesh, fadeState, disabledState)
{
    XBox(border, fillColor);
}

XBox::XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState, bool border, glm::vec4 fillColor, glm::vec4 borderColor) : Xidget(pos, size, mesh, fadeState, disabledState)
{
    XBox(border, fillColor, borderColor);
}

XBox::~XBox()
{
    //
}

glm::vec4 XBox::getFillColor()
{
    return color;
}

void XBox::setFillColor(glm::vec4 c)
{
    color = c;
}

void XBox::toggleBorder()
{
    setBorderState(!hasBorder());
}

bool XBox::hasBorder()
{
    return false;
}

void XBox::setBorderState(bool state)
{
    border = state;
}

glm::vec4 XBox::getBorderColor()
{
    return glm::vec4();
}

void XBox::setBorderColor(glm::vec4 color)
{
    //
}
