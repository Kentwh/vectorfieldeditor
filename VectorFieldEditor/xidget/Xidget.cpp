#include "Xidget.h"

Xidget::Xidget()
{
    
}

Xidget::Xidget(glm::vec2 pos)
{
    setDesiredPosition(pos);
}

Xidget::Xidget(glm::vec2 pos, glm::vec2 size)
{
    setDesiredPosition(pos);
    setDesiredSize(size);
}

Xidget::Xidget(glm::vec2 pos, glm::vec2 size, Mesh& mesh)
{
    setDesiredPosition(pos);
    setDesiredSize(size);
    setMesh(mesh);
}

Xidget::Xidget(glm::vec2 pos, glm::vec2 size, Mesh & mesh, bool fadeState)
{
    setDesiredPosition(pos);
    setDesiredSize(size);
    setMesh(mesh);
    setFadeState(fadeState);
}

Xidget::Xidget(glm::vec2 pos, glm::vec2 size, Mesh & mesh, bool fadeState, bool disableState)
{
    setDesiredPosition(pos);
    setDesiredSize(size);
    setMesh(mesh);
    setFadeState(fadeState);
    setDisableState(disableState);
}

Xidget::Xidget(XidgetAttrib& attrib)
{
    setDesiredPosition(attrib.position);
    setDesiredSize(attrib.size);
    setMesh(attrib.mesh);
    setFadeState(attrib.fadeState);
    setDisableState(attrib.disabledState);
}

Xidget::Xidget(Xidget& xidget)
{
    setDesiredPosition(xidget.getPosition());
    setDesiredSize(xidget.getSize());
    setMesh(xidget.getMesh());
    setFadeState(xidget.isFaded());
    setDisableState(xidget.isDisabled());
}

Xidget::Xidget(Xidget* xidget)
{
    setDesiredPosition(xidget->getPosition());
    setDesiredSize(xidget->getSize());
    setMesh(xidget->getMesh());
    setFadeState(xidget->isFaded());
    setDisableState(xidget->isDisabled());
}

Xidget::~Xidget()
{
    //
}

void Xidget::OnClicked(ClickCallback clickCallback)
{
    this->click = clickCallback;
}

void Xidget::OnHover(HoverCallback hoverCallback)
{
    this->hover = hoverCallback;
}

glm::vec2 Xidget::getPosition()
{
    return position;
}

void Xidget::setDesiredPosition(glm::vec2 pos)
{
    this->position = pos;
}

glm::vec2 Xidget::getSize()
{
    return size;
}

void Xidget::setDesiredSize(glm::vec2 s)
{
    this->size = s;
}

Mesh Xidget::getMesh()
{
    return  mesh;
}

void Xidget::setMesh(Mesh m)
{
    this->mesh = m;
}

void Xidget::toggleFade()
{
    setFadeState(!isFaded());
}

bool Xidget::isFaded()
{
    return fadeState;
}

void Xidget::setFadeState(bool state)
{
    this->fadeState = state;
}

void Xidget::toggleDisable()
{
    if (isFaded()) setDisableState(!isDisabled());
    else
    {
        setFadeState(!isFaded());
        setDisableState(!isDisabled());
    }
}

bool Xidget::isDisabled()
{
    return disabledState;
}

void Xidget::setDisableState(bool state)
{
    this->disabledState = state;
    setFadeState(state);
}

glm::vec2 Xidget::getDesiredPosition()
{
    return desired;
}

glm::vec2 Xidget::getDesiredSize()
{
    return glm::vec2();
}

XidgetType Xidget::getType()
{
    return XidgetType();
}

void Xidget::operator=(Xidget& xidget)
{
    this->mesh = xidget.getMesh();
    this->position = xidget.getPosition();
    this->desiredSize = xidget.getDesiredPosition();
    this->size;
    this->fadeState = false;
    this->disabledState = false;
    this->click = nullptr;
    this->hover = nullptr;
}

void Xidget::operator=(Xidget* xidget)
{
}
