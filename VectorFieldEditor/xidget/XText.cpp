#include "SText.h"

vfe::SText::SText() : vfe::Slate()
{

}

vfe::SText::SText(sf::RenderWindow* window) : vfe::Slate(window)
{
}

vfe::SText::~SText()
{

}

void vfe::SText::draw()
{
}

void vfe::SText::OnClicked(sf::Vector2i mPos)
{
}

void vfe::SText::OnHover(sf::Vector2i mPos)
{
}

std::string vfe::SText::getText()
{
    return text;
}

void vfe::SText::setText(std::string txt)
{
    text = txt;
}

sf::Color vfe::SText::getColor()
{
    return color;
}

void vfe::SText::setColor(sf::Color color)
{
    this->color = color;
}

SlateType vfe::SText::getType()
{
    return SlateText; // SText slate type = 4
}

void vfe::SText::operator=(Slate & slate)
{
}
