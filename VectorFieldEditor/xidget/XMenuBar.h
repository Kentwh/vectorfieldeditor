#pragma once
#include "SMenu.h"

namespace vfe
{
    class SMenuBar : public vfe::SMenu
    {
    public:
        SMenuBar();
        SMenuBar(sf::RenderWindow* window);
        virtual ~SMenuBar();

        // Draw function
        virtual void draw();

        // Pure OnClicked event function, custom body
        virtual void OnClicked(sf::Vector2i mPos);

        // Pure OnHover event function, custom body
        virtual void OnHover(sf::Vector2i mPos);

        // Attach slate to menu
        void attachSlate(Slate* slate);

        // Detach slate from menu
        void detachSlate(int index);

        // Get slate pointer by index
        Slate* getButtonByIndex(int index);

        // Get slate type
        virtual SlateType getType();

        virtual void operator=(Slate& slate);
    protected:
        //
    private:
        //
    };
}
