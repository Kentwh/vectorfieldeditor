#pragma once
#include "SBox.h"
#include <vector>

namespace vfe
{
    class SMenu : public vfe::SBox
    {
    public:
        SMenu();
        SMenu(sf::RenderWindow* window);
        virtual ~SMenu();

        // Draw function
        virtual void draw();

        // Pure OnClicked event function, custom body
        virtual void OnClicked(sf::Vector2i mPos);

        // Pure OnHover event function, custom body
        virtual void OnHover(sf::Vector2i mPos);

        // Add slate element
        void addElement(Slate slate);

        // Remove slate element by index
        void removeElementByIndex(int index);

        // Remove slate element by id
        bool removeElementById(SlateId id);

        // Get Slate by index
        Slate* getSlateByIndex(int index);

        // Get Slate by id
        Slate* getSlateById(SlateId index);

        // Get slate type
        virtual SlateType getType();

        virtual void operator=(Slate& slate);
    protected:
        std::vector<Slate> elements;
    private:
<<<<<<< HEAD
        Slate* findSlateById(SlateId id);
=======
>>>>>>> 5fe701e7ef06c5cf920b4f0e5804ff61574103e0
    };
}
