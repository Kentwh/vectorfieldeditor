#include "XButton.h"

vfe::XButton::XButton() : vfe::XBox()
{

}

vfe::XButton::XButton(sf::RenderWindow* window) : vfe::XBox(window)
{
}

vfe::XButton::~XButton()
{

}

void vfe::XButton::draw()
{
}

void vfe::XButton::OnClicked(sf::Vector2i mPos)
{
}

void vfe::XButton::OnHover(sf::Vector2i mPos)
{
}

vfe::Xidget vfe::XButton::getXidget()
{
    return Xidget();
}

void vfe::XButton::setXidget(Xidget xidget)
{
    this->xidget = xidget;
}

void vfe::XButton::operator=(Slate& slate)
{
    
}

SlateType vfe::XButton::getType()
{
    return SlateButton; // XButton, xidget type = 3
}
