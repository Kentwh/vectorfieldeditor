#include "SMenu.h"

vfe::SMenu::SMenu() : vfe::SBox()
{

}

vfe::SMenu::SMenu(sf::RenderWindow* window) : vfe::SBox(window)
{
}

vfe::SMenu::~SMenu()
{

}

void vfe::SMenu::draw()
{
}

/*void vfe::SMenu::OnClicked(sf::Vector2i mPos)
{
}

void vfe::SMenu::OnHover(sf::Vector2i mPos)
{
}*/

void vfe::SMenu::addElement(Slate slate)
{
    elements.push_back(slate);
}

void vfe::SMenu::removeElementByIndex(int index)
{
    elements.erase(elements.begin() + index);
}

bool vfe::SMenu::removeElementById(SlateId id)
{
    for (int i = 0; i < elements.capacity(); i++)
    {
        if (elements[i].getId() == id)
        {
            elements.erase(elements.begin() + i);
            return true;
        }
    }
    return false;
}

vfe::Slate* vfe::SMenu::getSlateByIndex(int index)
{
    return &elements[index];
}

vfe::Slate* vfe::SMenu::getSlateById(SlateId id)
{
    return findSlateById(id);
}

vfe::Slate* vfe::SMenu::findSlateById(SlateId id)
{
    for (Slate slate : elements)
    {
        if (slate.getId() == id) return &slate;
    }
    return nullptr;
}

SlateType vfe::SMenu::getType()
{
    return SlateMenu; // SMenu slate type = 5
}

void vfe::SMenu::operator=(Slate& slate)
{
}
