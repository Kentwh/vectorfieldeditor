#pragma once
#include <glm\glm.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include "Slate.h"

class VectorField : public Slate
{
public:
    VectorField(std::string fileName);
    virtual ~VectorField();

    // Draw function
    virtual void draw();

    // Pure OnClicked event function, custom body
    virtual void OnClicked(glm::vec2 mPos) = 0;

    // Pure OnHover event function, custom body
    virtual void OnHover(glm::vec2 mPos) = 0;
protected:
    //
private:
    glm::vec3* vField;
};

