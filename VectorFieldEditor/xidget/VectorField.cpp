#include "VectorField.h"

VectorField::VectorField(std::string fileName)
{
    std::ifstream file(fileName);
    glm::fvec3 dim;
    glm::fvec3 temp;

    if (file.is_open())
    {
        for (int a = 0; a < 3; a++)
        {
            file >> dim.x;
            file.ignore();
        }
        file.ignore(); 
        for (int a = 0; a < 3; a++)
        {
            file >> dim.y;
            file.ignore();
        }
        file.ignore(); 
        for (int a = 0; a < 3; a++)
        {
            file >> dim.z;
            file.ignore();
        }
        file.ignore();
        
        vField = new glm::vec3[dim.x * (-dim.y) * dim.z];

        for (int z = 0; z < (int)dim.x; z++)
        {
            for (int y = 0; y < (int)dim.x; y++)
            {
                for (int x = 0; x < (int)dim.x; x++)
                {
                    file >> temp.x; file.ignore(); file >> temp.y; file.ignore(); file >> temp.z; file.ignore(2);
                    vField[int(x + dim.x * (y + dim.z * z))] = temp;
                }
            }
        }
    }
}

VectorField::~VectorField()
{
    delete[] vField;
}

void VectorField::draw()
{

}
