#pragma once
#include "Xidget.h"

class XBox : public Xidget
{
public:
    XBox();
    XBox(bool border);
    XBox(bool border, glm::vec4 fillColor);
    XBox(bool border, glm::vec4 fillColor, glm::vec4 borderColor);
    XBox(glm::vec2 pos);
    XBox(glm::vec2 pos, glm::vec2 size);
    XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh);
    XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState);
    XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState);
    XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState, bool border);
    XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState, bool border, glm::vec4 fillColor);
    XBox(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState, bool border, glm::vec4 fillColor, glm::vec4 borderColor);
    virtual ~XBox();

    // Callback OnClicked event function
    virtual void OnClicked(ClickCallback clickCallback);
    
    //Callback OnHover event function
    virtual void OnHover(HoverCallback hoverCallback);

    glm::vec4 getFillColor();
    void setFillColor(glm::vec4 c);
    void toggleBorder();
    bool hasBorder();
    void setBorderState(bool state);
    glm::vec4 getBorderColor();
    void setBorderColor(glm::vec4 color);

    // Get xidget type
    virtual XidgetType getType();
    
    virtual void operator=(Xidget& xidget);
    
protected:
    bool border = false;
    glm::vec4 fillColor;
    glm::vec4 borderColor;
private:
    //
};

