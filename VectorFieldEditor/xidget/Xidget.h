#pragma once
#include "../Global.h"
#include "../Mesh.h"

/*---------- Event handling ----------*/

// Event is handeled
#define HANDLED 0x01
// Event didn't get handled
#define UHANDLED 0x02
// Event handling failed
#define HANDLING_FAILED 0x03
// Event handling completed with warnings
#define HANDLED_WARNINGS 0x04

/*---------- Child-Parent positioning behavior ----------*/

// Automatic positioning, ignores desired positioning
#define AUTO_POS 0x11
// Ignores parent positioning, completely independent from parent
#define INDEPENDENT_POS 0x12
// Greater control over positioning within parent.
// Parent size will become outer constraints
// Left = -1, Right = 1, Up = 1 and Bottom = -1.
// !!NOTE!! -> NOT APPLICABLE TO CUSTOM SHAPES!
#define INTERNALY_INDEPENDENT_POS 0x13

/*---------- Child-Parent size behavior ----------*/

// Automatic sizing, ignores desired size, Encloses content
#define AUTO_SIZE 0x20
// Ignores parent size conditions, completely independent from parent
#define INDEPENDENT_SIZE 0x21
// Ignores spesified ("desired") size, fills parent
#define FILL_PARENT 0x22
// Ignores parent specified size
#define INTERNALY_INDEPENDENT_SIZE 0x23


typedef int(*ClickCallback)(glm::vec2 pos);
typedef int(*HoverCallback)(glm::vec2 pos);

typedef int XidgetType;

struct XidgetAttrib
{
    Mesh mesh;
    glm::vec2 position;
    glm::vec2 size;
    bool fadeState = false;
    bool disabledState = false;
};

class Xidget
{
public:
    Xidget();
    Xidget(glm::vec2 pos);
    Xidget(glm::vec2 pos, glm::vec2 size);
    Xidget(glm::vec2 pos, glm::vec2 size);
    Xidget(glm::vec2 pos, glm::vec2 size, Mesh& mesh);
    Xidget(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState);
    Xidget(glm::vec2 pos, glm::vec2 size, Mesh& mesh, bool fadeState, bool disabledState);
    Xidget(XidgetAttrib& attrib);
    Xidget(Xidget& xidget);
    Xidget(Xidget* xidget);
    virtual ~Xidget();

    virtual void draw();

    // Callback OnClicked event function
    virtual void OnClicked(ClickCallback clickCallback);

    //Callback OnHover event function
    virtual void OnHover(HoverCallback hoverCallback);

    // Gets the current position of the Xidget
    glm::vec2 getPosition();

    // sets the current position of the Xidget, 
    void setDesiredPosition(glm::vec2 pos);

    // Get the current size of the Xidget
    glm::vec2 getSize();

    // Sets the desired size, if possible the Xidget will have this size
    void setDesiredSize(glm::vec2 s);

    // Get the crrent mesh bound to this Xidget
    Mesh getMesh();

    // Sets/Binds the given mesh as the current mesh to draw
    void setMesh(Mesh m);

    // Toggles the fade effect
    void toggleFade();

    // Gets the fade effect state
    bool isFaded();

    // Sets the state of the fade effect
    void setFadeState(bool state);

    // Toggles the disabling effect aswell as the fade
    void toggleDisable();

    // Gets the state of the disable feature
    bool isDisabled();

    // Sets the state of the disable feature aswell as the fade effect
    void setDisableState(bool state);

    // Get xidget type
    virtual XidgetType getType();

    virtual void operator=(Xidget& xidget);
    virtual void operator=(Xidget* xidget);
protected: 
    Mesh mesh = Mesh(GL_TRIANGLES);
    glm::vec2 desiredPos = glm::vec2(0.0f);
    glm::vec2 position = glm::vec2(0.0f);
    glm::vec2 desiredSize = glm::vec2(0.0f);
    glm::vec2 size = glm::vec2(0.0f);
    bool fadeState = false;
    bool disabledState = false;

    ClickCallback click = nullptr;
    HoverCallback hover = nullptr;
private: 
    // Gets the current desired position of the Xidget, 
    glm::vec2 getDesiredPosition();

    // Sets the desired size
    glm::vec2 getDesiredSize();
};

