#include "SMenuBar.h"

vfe::SMenuBar::SMenuBar() : vfe::SMenu()
{

}

vfe::SMenuBar::SMenuBar(sf::RenderWindow* window) : vfe::SMenu(window)
{
}

vfe::SMenuBar::~SMenuBar()
{

}

void vfe::SMenuBar::draw()
{
}

void vfe::SMenuBar::OnClicked(sf::Vector2i mPos)
{
}

void vfe::SMenuBar::OnHover(sf::Vector2i mPos)
{
}

void vfe::SMenuBar::attachSlate(Slate * slate)
{
}

void vfe::SMenuBar::detachSlate(int index)
{
}

vfe::Slate* vfe::SMenuBar::getButtonByIndex(int index)
{
    return nullptr;
}

SlateType vfe::SMenuBar::getType()
{
    return SlateMenuBar; // SMenuBar slate type = 6
}

void vfe::SMenuBar::operator=(Slate& slate)
{
}
