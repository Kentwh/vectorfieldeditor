#version 330

attribute vec3 position;
attribute vec4 color;

varying vec4 color0;

//uniform mat4 transform;

void main()
{
    //gl_Position = /*transform * */vec4(position, 1.0);
    gl_Position = vec4(position, 1.0);
    color0 = color;
}